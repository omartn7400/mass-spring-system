using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsSpawner : MonoBehaviour
{
    private const float g = 9.81f;
    public float x_Start, y_Start;
    public int columnLength, rowLength;
    public float x_Space, y_Space;
    public GameObject prefab;
    Vector2 gridSize;
    public float amortissment = 1.5f;
    public float longueur = 2;
    public float masse = 2;
    public float dumpingCoef = 2;

    struct sphereDetails
    {
        public Vector3 acceleration;
        public Vector3 force;
        public GameObject gameObject;
    }
    sphereDetails[,] gridOfGameObjects;

    float timePassed;

    // Use this for initialization
    void Start()
    {
        gridSize = new Vector2(columnLength, rowLength);
        gridOfGameObjects = new sphereDetails[(int)gridSize.x, (int)gridSize.y];
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int y = 0; y < gridSize.y; y++)
            {
                float pos_x = x_Space * (x_Start + x);
                float pos_z = y_Space * (y_Start - y);
                var sphere = Instantiate(prefab, new Vector3(pos_x, 0, pos_z), Quaternion.identity);
                sphere.name = x.ToString() + y.ToString();
                gridOfGameObjects[x, y].gameObject = sphere;
                gridOfGameObjects[x, y].force = Vector3.zero;
                gridOfGameObjects[x, y].acceleration = Vector3.zero;
            }
        }

        
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timePassed += Time.fixedDeltaTime;
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int y = 0; y < gridSize.y; y++)
            {
                //Structural Spring
                if (x < gridSize.x - 1)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x + 1, y].gameObject, 1);
                }
                if (x > 0)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x - 1, y].gameObject, 1);
                }
                if (y < gridSize.y - 1)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x, y + 1].gameObject, 1);
                }
                if (y > 0)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x, y - 1].gameObject, 1);
                }


                //Shear Spring
                if (x < gridSize.x - 1 && y < gridSize.y - 1)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x + 1, y + 1].gameObject, Mathf.Sqrt(2));
                }
                if (x < gridSize.x - 1 && y > 0)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x + 1, y - 1].gameObject, Mathf.Sqrt(2));
                }
                if (x > 0  && y > 0)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x - 1, y - 1].gameObject, Mathf.Sqrt(2));
                }
                if(x > 0 && y < gridSize.y - 1)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x - 1, y + 1].gameObject, Mathf.Sqrt(2));
                }



                //flexion Spring
                if (x < gridSize.x - 2)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x + 2, y].gameObject, 2);
                }
                if (x > 1)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x - 2, y].gameObject, 2);
                }
                if (y < gridSize.y - 2)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x, y + 2].gameObject, 2);
                }
                if (y > 1)
                {
                    gridOfGameObjects[x, y].force += CalculateForce(gridOfGameObjects[x, y].gameObject, gridOfGameObjects[x, y - 2].gameObject, 2);
                }

                
            }
        }
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int y = 0; y < gridSize.y; y++)
            {
                gridOfGameObjects[x, y].gameObject.transform.position += ApplyForce(gridOfGameObjects[x, y].force, x, y, gridOfGameObjects[x, y].gameObject);
            }
        }

    }

    Vector3 ApplyForce(Vector3 force,int x, int y, GameObject gameObject)
    {
        Vector3 Velocity;
        Vector3 newPosition;
        //if ((x==0 && y==0) ||(x == 0 && y == 9) || (x == 0 && y == 4))
        if (x==0)
        {
            gridOfGameObjects[x, y].acceleration = Vector3.zero;
            
        }
        else
        {
            gridOfGameObjects[x, y].acceleration = (-dumpingCoef * gridOfGameObjects[x, y].acceleration * Time.fixedDeltaTime  + force / timePassed + new Vector3(0,-g*masse, 0))/masse;
            

        }

        Velocity = gridOfGameObjects[x, y].acceleration * Time.fixedDeltaTime * 10;
        newPosition = Velocity * Time.fixedDeltaTime * 10;
        
        return newPosition;
    }

    Vector3 CalculateForce(GameObject gameObject1, GameObject gameObject2,float longRessort)
    {
        Vector3 l;
        Vector3 forces;
        l = gameObject2.transform.position - gameObject1.transform.position;
        Debug.DrawLine(gameObject2.transform.position, gameObject1.transform.position, Color.white);
        forces = amortissment * (l - (longRessort*longueur * (l / l.magnitude)));

        
        return forces;
    }
}
